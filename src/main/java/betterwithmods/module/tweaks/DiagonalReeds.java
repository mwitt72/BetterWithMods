package betterwithmods.module.tweaks;

import betterwithmods.common.BWMBlocks;
import betterwithmods.common.blocks.BlockReed;
import betterwithmods.common.world.gen.feature.WorldGenReed;
import betterwithmods.module.Feature;
import net.minecraft.block.Block;
import net.minecraft.world.biome.BiomeDecorator;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.terraingen.BiomeEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class DiagonalReeds extends Feature {

    @GameRegistry.ObjectHolder("minecraft:reeds")
    private static Block REEDS;

    @Override
    public void preInit(FMLPreInitializationEvent event) {
        REEDS = new BlockReed().setRegistryName("minecraft:reeds").setTranslationKey("reeds");
        BWMBlocks.registerBlock(REEDS, null);
        MinecraftForge.TERRAIN_GEN_BUS.register(this);
    }

    @Override
    public boolean hasSubscriptions() {
        return true;
    }

    @Override
    public String getFeatureDescription() {
        return "Reeds check both cardinal and ordinal directions for valid placement.";
    }

    @SubscribeEvent
    public void onCreateDecorator(BiomeEvent.CreateDecorator event) {
        BiomeDecorator decorator = event.getOriginalBiomeDecorator();
        decorator.reedGen = new WorldGenReed();
    }

}

